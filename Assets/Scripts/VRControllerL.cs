﻿using UnityEngine;
using System.Collections;

public class VRControllerL : MonoBehaviour {
    public GameObject GrabTarget_ = null;
    private GameObject GrabObject_ = null;

    // Use this for initialization
    void Start () {
        var trackedController = gameObject.GetComponent<SteamVR_TrackedController>();
        trackedController.TriggerClicked += TrackedController_TriggerClicked;
        trackedController.TriggerUnclicked += TrackedController_TriggerUnclicked;
    }

    private void TrackedController_TriggerUnclicked(object sender, ClickedEventArgs e) {
        ungrabObject();
    }

    private void TrackedController_TriggerClicked(object sender, ClickedEventArgs e) {
        grabObject(GrabTarget_);
    }

    private void grabObject(GameObject o) {
        Debug.Log("Grab TargetObject");
        GrabObject_ = o;

        FixedJoint fj = gameObject.GetComponent<FixedJoint>();
        fj.connectedBody = o.GetComponent<Rigidbody>();
    }

    private void ungrabObject() {
        Debug.Log("UnGrab TargetObject");
        if (GrabObject_ != null) {
            FixedJoint fj = gameObject.GetComponent<FixedJoint>();
            fj.connectedBody = null;
        }

        GrabObject_ = null;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
