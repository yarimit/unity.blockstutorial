﻿using UnityEngine;
using System.Collections;

public class VRController : MonoBehaviour {
    private GameObject GrabObject_ = null;
    private SteamVR_TrackedObject TrackedObject_;

    private int CurrentPrefab_;
    private GameObject[] Prefabs_;
    public GameObject BlockPrefab_;
    public GameObject SpherePrefab_;
    public GameObject UnityPrefab_;
    public GameObject MarunPrefab_;

	// Use this for initialization
	void Start () {
        var trackedController = gameObject.GetComponent<SteamVR_TrackedController>();
        trackedController.TriggerClicked += TrackedController_TriggerClicked;
        trackedController.TriggerUnclicked += TrackedController_TriggerUnclicked;
        trackedController.PadClicked += TrackedController_PadClicked;
        trackedController.Gripped += TrackedController_Gripped;
        //trackedController.TriggerClicked += new ClickedEventHandler(TrackedController_TriggerClicked);
        //trackedController.TriggerUnclicked += new ClickedEventHandler(TrackedController_TriggerClicked);

        Prefabs_ = new GameObject[] { BlockPrefab_, SpherePrefab_, UnityPrefab_, MarunPrefab_ };
        CurrentPrefab_ = 0;

        TrackedObject_ = GetComponent<SteamVR_TrackedObject>();
    }

    private void TrackedController_Gripped(object sender, ClickedEventArgs e) {
        if(CurrentPrefab_ < Prefabs_.Length - 1) {
            CurrentPrefab_++;
        } else {
            CurrentPrefab_ = 0;
        }
    }

    private void TrackedController_PadClicked(object sender, ClickedEventArgs e) {
        //Instantiate(BlockPrefab_, new Vector3(0, 1, 1), Quaternion.identity);

        Vector3 pos = transform.position + (transform.forward * 2);
        Instantiate(Prefabs_[CurrentPrefab_], pos, Quaternion.identity);
    }

    private void TrackedController_TriggerUnclicked(object sender, ClickedEventArgs e) {
        GameObject to = GrabObject_;

        ungrabObject();

        var device = SteamVR_Controller.Input((int)TrackedObject_.index);
        if (to != null) {
            var rigidbody = to.GetComponent<Rigidbody>();
            var origin = TrackedObject_.origin ? TrackedObject_.origin : TrackedObject_.transform.parent;
            if (origin != null) {
                rigidbody.velocity = origin.TransformVector(device.velocity);
                rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
            }
            else {
                rigidbody.velocity = device.velocity;
                rigidbody.angularVelocity = device.angularVelocity;
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;

        }

    }

    private void TrackedController_TriggerClicked(object sender, ClickedEventArgs e) {
        Ray raycast = new Ray(transform.position, transform.forward);   // コントローラからの直線Ray

        RaycastHit hitInfo;
        var hit = Physics.Raycast(raycast, out hitInfo, 10);

        if(hit) {
            GameObject pointingObject = hitInfo.transform.gameObject;
            grabObject(pointingObject);
        }
    }

    private void grabObject(GameObject o) {
        GrabObject_ = o;

        FixedJoint fj = gameObject.GetComponent<FixedJoint>();
        fj.connectedBody = o.GetComponent<Rigidbody>();
    }

    private void ungrabObject() {
        if (GrabObject_ != null) {
            FixedJoint fj = gameObject.GetComponent<FixedJoint>();
            fj.connectedBody = null;
        }

        GrabObject_ = null;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
